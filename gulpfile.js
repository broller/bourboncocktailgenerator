/* File: gulpfile.js */

// grab our packages
var gulp   = require('gulp'),
    jshint = require('gulp-jshint'),
	sourcemaps = require('gulp-sourcemaps'),
	uglify = require('gulp-uglify'),
	concat = require('gulp-concat');

// define the default task and add the watch task to it
gulp.task('default', ['watch']);

// configure the jshint task
gulp.task('jshint', function() {
  return gulp.src('scripts/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('build-js', function() {
  return gulp.src(['scripts/app.js', 'scripts/filters/cocktailFilter.js', 'scripts/controllers/cocktailController.js', 'scripts/controllers/editorController.js'])
    .pipe(sourcemaps.init())
      .pipe(concat('bundle.js'))
	  .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('scripts'));
});

// configure which files to watch and what tasks to use on file changes
gulp.task('watch', function() {
  gulp.watch('scripts/**/*.js', ['jshint']);
});