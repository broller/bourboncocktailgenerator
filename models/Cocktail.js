var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var cocktailSchema = new Schema({
	name: String,
	ingredients: [{id: {type: mongoose.Schema.Types.ObjectId, ref: 'Ingredient'}, contents: String}],
	directions: String
});

module.exports = mongoose.model('Cocktail', cocktailSchema);