var express = require('express');
var router = express.Router();
var passport = require('passport');
var Ingredient  = require('../models/Ingredient'); 
var Cocktail = require('../models/Cocktail');
var Account = require('../models/Account');

/* GET home page. */
router.get('/', function(req, res, next) {
	if(req.isAuthenticated())
  		res.render('index', { title: 'Bourbon Cocktail Generator', user: req.user });
  	else
  		res.render('index', { title: 'Bourbon Cocktail Generator', user: null });
});

router.get('/editor', function(req, res, next){
	if(req.isAuthenticated())
		res.render('editor', {title: 'Bourbon Cocktail Generator', user: req.user});
	else
		res.redirect('/login');
});

router.get('/ingredients', function(req, res, next){
	Ingredient.find(function(err, ingredients){
		if(err)
			res.send(err);

		return res.json(ingredients);
	});

});

router.get('/cocktails', function(req, res, next){
	Cocktail.find(function(err, cocktails){
		if(err)
			res.send(err);
		
		return res.json(cocktails);
	});
});

router.post('/ingredients', function(req, res){
	if(req.isAuthenticated()) {
		var ing = new Ingredient();
		ing.name = req.body.name;

		ing.save(function(err){
			if(err)
				res.send(err);

			return res.json({message: "Ingredient created!"});
		});
	}

	return res;
});

router.post('/cocktails', function(req, res){
	if(req.isAuthenticated()){
		var cocktail = new Cocktail();
		cocktail.name = req.body.name;
		cocktail.ingredients = [];

		var obj = req.body.ingredients;
		for (var key in obj) {
			console.log(key);
  			if (obj.hasOwnProperty(key)) {
    			cocktail.ingredients.push(obj[key]);
    		}
  		}

		cocktail.directions = req.body.directions;
		console.log("Cocktail " + cocktail);
		cocktail.save(function(err){
			if(err)
				res.send(err);

			return res.json({message: "Cocktail saved"});
		});
	}

	return res;
});

router.delete('/cocktails', function(req, res){
	if(req.isAuthenticated()){
		var cocktail = Cocktail.find({_id: req.body._id});
		cocktail.remove(function (err, cocktail) {
  			if (err) return handleError(err);
  			console.log("In remove " + cocktail);
  			Cocktail.findById(cocktail._id, function (err, cocktail) {
    			console.log(cocktail); // null
    			return res.json({message: "success"});
  			});
  			
		});
		
		return res;
	}
	else
		res.redirect('/');
});
/*
router.get('/register', function(req, res) {
    res.render('register', { });
});

router.post('/register', function(req, res) {
    Account.register(new Account({ username : req.body.username }), req.body.password, function(err, account) {
        if (err) {
            return res.render('register', { account : account });
        }

        passport.authenticate('local')(req, res, function () {
        	console.log("Redirect")
            return res.render('editor', { title: 'Bourbon Cocktail Generator', user: req.user });
        });
    });
});
*/
router.get('/login', function(req, res) {
    res.render('login', { user : req.user });
});

router.post('/login', passport.authenticate('local', {
        successRedirect : '/editor', // redirect to the secure profile section
        failureRedirect : '/register', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
}));

router.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
});

module.exports = router;