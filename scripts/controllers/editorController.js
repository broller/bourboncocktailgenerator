//var cocktailApp = angular.module('cocktailApp');

cocktailApp.controller('editorController', ['Flash','$scope','$http', function (Flash, $scope, $http) {
   $scope.ingredientList = [];

  $http.get('/ingredients').success(function(data) {
    $scope.ingredients = data;
  });

  $http.get('/cocktails').success(function(data) {
    $scope.cocktails = data;
  });

   $scope.getMatches = function(text){
    var arr = [];
    $scope.ingredients.forEach(function(ingredient){
      if(ingredient.name.toLowerCase().includes(text.toLowerCase()) &&
        !$scope.ingredientList.includes(ingredient))
        arr.push(ingredient);
    });

    return arr;
  };

  $scope.save = function(valid, ingredient){
	if(valid){
		$http.post('/ingredients', ingredient).success(function(data){
			$scope.ingredient = {};
			$scope.ingredientForm.$setPristine();
			$scope.ingredientForm.$setUntouched();
			$scope.success();
		});
	}
  };

  $scope.addToList = function(ingredient){
    $scope.ingredientList.push(ingredient);
    $scope.activeList = true;
	
	var index = $scope.ingredients.indexOf(ingredient);
	if(index != -1)
		$scope.ingredients.splice(index, 1);
  };

  $scope.success = function(){
		var message = "Your submission has successfully been saved to the bourbon cocktail generator database." ;
		var id = Flash.create('success', message, 5000, {class: 'custom-class', id: 'custom-id'}, true);
  };
  
  $scope.saveCocktail = function(valid, cocktail){
    console.log(valid);
	if(valid) {
		for(var i = 0; i < $scope.ingredientList.length; i++){
			cocktail.ingredients[i]["id"] = $scope.ingredientList[i]["_id"];
		}

		$http.post('/cocktails', cocktail).success(function(data){
			$scope.cocktail = {};
			$scope.ingredientList = [];
			$scope.cocktailForm.$setPristine();
			$scope.cocktailForm.$setUntouched();
			$scope.success();
		});
	}
	else {
		console.dir($scope.cocktailForm);
		console.log($scope.cocktailForm.$submitted && $scope.cocktailForm.cocktailDescription_0.$invalid);
	}
  };
  
	$scope.onSelect = function ($item, $model, $label) {
		$scope.addToList($model);
		$scope.cocktailIngredient = null;
	};

	$scope.clearItem = function(cocktail, phone){
		if($scope.ingredientList.length > 0){
			var index = $scope.ingredientList.indexOf(phone);
			if(index != -1){
				$scope.ingredientList.splice(index, 1);
				$scope.ingredients.push(phone);
			}
		}
	};
  
  $scope.deleteCocktail = function(cocktail){
    var config = {
      method: "DELETE",
      url: '/cocktails',
      data: cocktail,
      headers: {"Content-Type": "application/json;charset=utf-8"}
    };

    $http(config).then(function successCallback(response) {
    // this callback will be called asynchronously
    // when the response is available
      var index = $scope.cocktails.indexOf(cocktail);
      console.log(index);
      if(index != -1)
        $scope.cocktails.splice(index, 1);
  }, function errorCallback(response) {
    console.log("in failure");
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });
  };
}]);