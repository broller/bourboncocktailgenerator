//var cocktailApp = angular.module('cocktailApp');

cocktailApp.controller('cocktailController', ['$scope', '$http', function ($scope, $http) {
  $scope.ingredientList = [];
  $scope.activeList = false;

  $http.get('/ingredients').success(function(data) {
    $scope.ingredients = data;
  });

  $http.get('/cocktails').success(function(data) {
      $scope.cocktails = data;
  });
  
	$scope.onSelect = function ($item, $model, $label) {
		$scope.addToList($model);
		$scope.ingredient = null;
	};

  $scope.addToList = function(ingredient){
    $scope.ingredientList.push(ingredient);
    $scope.activeList = true;
	
	var index = $scope.ingredients.indexOf(ingredient);
	if(index != -1)
		$scope.ingredients.splice(index, 1);
  };

  $scope.clearAll = function(){
	for(var i = 0; i < $scope.ingredientList.length; i++)
		$scope.ingredients.push($scope.ingredientList[i]);
	
	$scope.ingredientList = [];
    $scope.activeList = false;
  };

  $scope.clearItem = function(phone){
    if($scope.ingredientList.length > 0){
      var index = $scope.ingredientList.indexOf(phone);
      if(index != -1){
        $scope.ingredientList.splice(index, 1);
		$scope.ingredients.push(phone);
      }
    
      if($scope.ingredientList.length == 0)
        $scope.activeList = false;
    }
  };
}]);
