//var cocktailApp = angular.module('cocktailApp');

cocktailApp.filter('cocktailFilter', function(){
  return function(cocktailList, ingredientList){
    var filtered = [];

    if(cocktailList != undefined && ingredientList != undefined){
        cocktailList.forEach(function(cocktail){  

			for(var i = 0; i < cocktail.ingredients.length; i++){
				for(var j = 0; j < ingredientList.length; j++){
					if(ingredientList[j]["_id"] == cocktail.ingredients[i]["id"]){
						if(!filtered.includes(cocktail)){
							filtered.push(cocktail);
							break;
						}
					}
				}
			}
		});
    }

	inList(filtered, ingredientList);
	
    return filtered;
  }
  
  function inList(list, ingredientList){
	  list.forEach(function(cocktail){
		var count = 0;
		for(var i = 0; i < cocktail.ingredients.length; i++){
			for(var j = 0; j < ingredientList.length; j++){
				if(ingredientList[j]["_id"] == cocktail.ingredients[i]["id"])  {
					cocktail.ingredients[i]["included"] = true;
					count++;
					break;
				}
				else
					cocktail.ingredients[i]["included"] = false;
			}
		}
		
		if(count == cocktail.ingredients.length){
			cocktail.heading = '\u2714';
			cocktail.complete = true;
			cocktail.missing = 0;
		}
		else {
			cocktail.heading =  "(missing " + (cocktail.ingredients.length - count) + ((cocktail.ingredients.length - count) == 1 ? " ingredient" : " ingredients") + ")";
			cocktail.complete = false;
			cocktail.missing = cocktail.ingredients.length - count;
		}
	  });	
  }
});