var cocktailApp = angular.module('cocktailApp', ['ui.bootstrap']);

cocktailApp.filter('cocktailFilter', function(){
  return function(cocktailList, ingredientList){
    var filtered = [];

    if(cocktailList != undefined && ingredientList != undefined){
        cocktailList.forEach(function(cocktail){  

			for(var i = 0; i < cocktail.ingredients.length; i++){
				for(var j = 0; j < ingredientList.length; j++){
					if(ingredientList[j]["_id"] == cocktail.ingredients[i]["id"]){
						if(!filtered.includes(cocktail)){
							filtered.push(cocktail);
							break;
						}
					}
				}
			}
		});
    }

	inList(filtered, ingredientList);
	
    return filtered;
  }
  
  function inList(list, ingredientList){
	  list.forEach(function(cocktail){
		var count = 0;
		for(var i = 0; i < cocktail.ingredients.length; i++){
			for(var j = 0; j < ingredientList.length; j++){
				if(ingredientList[j]["_id"] == cocktail.ingredients[i]["id"])  {
					cocktail.ingredients[i]["included"] = true;
					count++;
					break;
				}
				else
					cocktail.ingredients[i]["included"] = false;
			}
		}
		
		if(count == cocktail.ingredients.length){
			cocktail.heading = '\u2714';
			cocktail.complete = true;
			cocktail.missing = 0;
		}
		else {
			cocktail.heading =  "(missing " + (cocktail.ingredients.length - count) + ((cocktail.ingredients.length - count) == 1 ? " ingredient" : " ingredients") + ")";
			cocktail.complete = false;
			cocktail.missing = cocktail.ingredients.length - count;
		}
	  });	
  }
});

cocktailApp.controller('cocktailController', function ($scope, $http) {
  $scope.ingredientList = [];
  $scope.activeList = false;

  $http.get('/ingredients').success(function(data) {
    $scope.ingredients = data;
  });

  $http.get('/cocktails').success(function(data) {
      $scope.cocktails = data;
  });
  
	$scope.onSelect = function ($item, $model, $label) {
		$scope.addToList($model);
		$scope.ingredient = null;
	};

  $scope.addToList = function(ingredient){
    $scope.ingredientList.push(ingredient);
    $scope.activeList = true;
	
	var index = $scope.ingredients.indexOf(ingredient);
	if(index != -1)
		$scope.ingredients.splice(index, 1);
  };

  $scope.clearAll = function(){
	for(var i = 0; i < $scope.ingredientList.length; i++)
		$scope.ingredients.push($scope.ingredientList[i]);
	
	$scope.ingredientList = [];
    $scope.activeList = false;
  };

  $scope.clearItem = function(phone){
    if($scope.ingredientList.length > 0){
      var index = $scope.ingredientList.indexOf(phone);
      if(index != -1){
        $scope.ingredientList.splice(index, 1);
		$scope.ingredients.push(phone);
      }
    
      if($scope.ingredientList.length == 0)
        $scope.activeList = false;
    }
  };

  $scope.measurementExists = function(ingredient){
    alert(ingredient.measurement);
    return ingredient.measurement != null;
  };
});

cocktailApp.controller('editorController', function ($scope, $http) {
   $scope.ingredientList = [];

  $http.get('/ingredients').success(function(data) {
    $scope.ingredients = data;
  });

  $http.get('/cocktails').success(function(data) {
    $scope.cocktails = data;
  });

   $scope.getMatches = function(text){
    var arr = [];
    $scope.ingredients.forEach(function(ingredient){
      if(ingredient.name.toLowerCase().includes(text.toLowerCase()) &&
        !$scope.ingredientList.includes(ingredient))
        arr.push(ingredient);
    });

    return arr;
  };

  $scope.save = function(ingredient){
    $http.post('/ingredients', ingredient).success(function(data){
	   $scope.ingredient = {};
       $scope.ingredientForm.$setPristine();
       $scope.ingredientForm.$setUntouched();
    });
  };

  $scope.addToList = function(ingredient){
    $scope.ingredientList.push(ingredient);
    $scope.activeList = true;
	
	var index = $scope.ingredients.indexOf(ingredient);
	if(index != -1)
		$scope.ingredients.splice(index, 1);
  };

  $scope.saveCocktail = function(cocktail){
    console.log(cocktail);
    for(var i = 0; i < $scope.ingredientList.length; i++){
      cocktail.ingredients[i]["id"] = $scope.ingredientList[i]["_id"];
    }

    $http.post('/cocktails', cocktail).success(function(data){
      
    });
  };
  
	$scope.onSelect = function ($item, $model, $label) {
		$scope.addToList($model);
		$scope.cocktailIngredient = null;
	};

	$scope.clearItem = function(cocktail, phone){
		if($scope.ingredientList.length > 0){
			var index = $scope.ingredientList.indexOf(phone);
			if(index != -1){
				$scope.ingredientList.splice(index, 1);
				$scope.ingredients.push(phone);
			}
		}
	};
  
  $scope.deleteCocktail = function(cocktail){
    console.log("cocktail:\t" + cocktail["name"]);

    var config = {
      method: "DELETE",
      url: '/cocktails',
      data: cocktail,
      headers: {"Content-Type": "application/json;charset=utf-8"}
    };

    $http(config).then(function successCallback(response) {
    // this callback will be called asynchronously
    // when the response is available
      var index = $scope.cocktails.indexOf(cocktail);
      console.log(index);
      if(index != -1)
        $scope.cocktails.splice(index, 1);
  }, function errorCallback(response) {
    console.log("in failure");
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });
    
  };
});